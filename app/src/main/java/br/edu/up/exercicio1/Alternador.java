package br.edu.up.exercicio1;

import android.content.Context;
import android.content.Intent;

public class Alternador {
    public static void alternar(Context context, String activity) {
        switch (activity){
            case "1":
                context.startActivity(new Intent(context, PrimeiraActivity.class));
                break;
            case "2":
                context.startActivity(new Intent(context, SegundaActivity.class));
                break;
            case "3":
                context.startActivity(new Intent(context, TerceiraActivity.class));
                break;
            case "4":
                context.startActivity(new Intent(context, QuartaActivity.class));
                break;
            case "5":
                context.startActivity(new Intent(context, QuintaActivity.class));
                break;
        }
    }
}
